﻿using System;
using System.Collections.Generic;

namespace WpfApplication1
{
    [Serializable]
    class PathModel
    {
        public string source { get; set; }
        public string target { get; set; }
        public List<string> targets { get; set; }
        public bool aboutRead { get; set; }

        public static PathModel defaultModel()
        {
            return new PathModel()
            {
                source = @"c:\YOURSOURCE\",
                target = @"c:\YOURTARGET\",
                targets = new List<string>(),
                aboutRead = false
            };
        }
    }
}
