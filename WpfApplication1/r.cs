﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows;

namespace WpfApplication1
{
    static class consoleController
    {
        //required for hide and show console, requires "using System.Runtime.InteropServices"
        //To get console output, go to Project properties and change output type to console instead of application
        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();
        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
        const int SW_HIDE = 0;
        const int SW_SHOW = 5;
        /////////////////////////////
        public static void showConsole()
        {
            var handle = GetConsoleWindow();
            ShowWindow(handle, SW_SHOW);
        }
        public static void hideConsole()
        {
            var handle = GetConsoleWindow();
            ShowWindow(handle, SW_HIDE);
        }
    }

    static class fileLog
    {
        public static string logPath;
        public static string fullLogPath = ""; //the exact file path
        public static int errorCount;
        public static string logFileName;
        public static int logFileCount; //is appended per session if over size limit to start a new file
        public static int sizeLimit = 10000000; //this is in bytes, when reach a new log is created
        private static bool createNew = true;

        public static List<string> getLogFileListForm()
        {
            List<string> log = new List<string>();
            log.Add("** Current log information, check back for new entries. **");
            log.Add("Log path: " + fullLogPath);
            log.Add("");
            using (var sr = new StreamReader(fullLogPath))
            {
                while (sr.Peek() >= 0)
                {
                    log.Add(sr.ReadLine());
                }
            }
            return log;
        }
        public static void outlog(string text, bool isError) //isError will mark as error count, otherwise just normal log
        {
            var msgBoxOn = false; //disable here directly to disable msgbox during error
            var errorTag = "";
            var completeMessage = "";

            if (createNew)
            {
                reinit();
            }

            //log is error type
            if (isError)
            {
                errorTag = "* ERROR * ";
                errorCount++;
            }

            try
            {
                using (StreamWriter filew = new StreamWriter(fullLogPath, true))
                {
                    completeMessage = errorTag + DateTime.Now.ToString("MM/dd/yy HH:mm:ss") + ": " + text;
                    filew.WriteLine(completeMessage);
                    filew.Close();
                }
            }
            catch
            {
                //r.v("Cannot log file");
            }
            if (msgBoxOn && isError)
            {
                r.v(completeMessage);
            }

            //sizelimit checker, if limit reach, a new file will be created.. file name is based on time down to seconds
            if (fullLogPath.Length > 0) //if fullLogPath is defined
            {
                if (new FileInfo(fullLogPath).Length > sizeLimit)//this is in bytes
                {
                    createNew = true;
                }
            }
        }

        static void reinit()
        {
            logPath = Directory.GetCurrentDirectory() + "\\Logs\\";
            logFileName = DateTime.Now.ToString("MMddyyHHmmss") + "_Logs";
            fullLogPath = logPath + logFileName + ".txt";
            createNew = false;
        }
        public static void expireLog(int length)
        {
            if (!Directory.Exists(logPath)) return;
            var info = new DirectoryInfo(logPath);
            FileInfo[] files = info.GetFiles().OrderBy(p => p.CreationTime).ToArray();
            for (int i = length; i < files.Length - 1; i++)
            {
                files[i].Delete();
            }
        }
    }
    class ri //non-static references
    {
        public string TruncateString(string text, int limit) //has static version
        {
            if (text.Length > limit - 1)
            {
                return text.Substring(0, limit) + "...";
            }
            else
            {
                return text;
            }
        }
        public string convertToASCII(string text)
        {
            return Encoding.ASCII.GetString(Encoding.ASCII.GetBytes(text));
        }
    }
    public static class r //static reference
    {
        //public static bool OnlyPostiveNegativeNumbers(string input)
        //{
        //    var re = new Regex(@"^\-?[0-9]\d{0,6}$"); //7 digits limit
        //    return re.IsMatch(input);
        //}
        public static string TruncateString(string text, int limit)
        {
            if (text.Length > limit - 1)
            {
                return text.Substring(0, limit) + "...";
            }
            else
            {
                return text;
            }
        }
        public static bool CheckFileCorrupted(string filePath)
        {
            var fi = new System.IO.FileInfo(filePath);
            try
            {
                if (System.IO.FileAttributes.ReadOnly == fi.Attributes) return false;
            }
            catch
            {
                return true;
            }
            return true;
        }
        public static string[] ListToArryString(List<string> original){
            string[] stringer = new string[original.Count];
            for (int i = 0; i < stringer.Length; i++)
            {
                stringer[i] = original[i];
            }
            return stringer;
        }

        /// <summary>
        /// Message box
        /// </summary>
        /// <param name="write"></param>
        public static void v(string write)
        {
            System.Windows.MessageBox.Show(write, "Warning");
        }

        public static void fp()
        {
            System.Console.Read();
        }

        public static bool fileExist(string fileLocation)
        {
            if (File.Exists(fileLocation))
            {
                return true;
            }
            return false;
        }

        public static bool folderExist(string folderLocation)
        {
            if (Directory.Exists(folderLocation))
            {
                return true;
            }
            return false;
        }

        public static int AvailableThreadpool()
        {
            int t1, t2;
            ThreadPool.GetAvailableThreads(out t1, out t2);
            return t1;
        }

        public static System.Windows.Visibility SetVisibility(bool isVisible)
        {
            if (isVisible) return Visibility.Visible;
            return Visibility.Hidden;
        }

        //copies all files and folder to location
        public static bool CopyFoldersRecursively(DirectoryInfo source, DirectoryInfo target)
        {
            //use argument like (new DirectoryInfo(originalPath), new DirectoryInfo(dumpPath))
            try
            {
                foreach (DirectoryInfo dir in source.GetDirectories())
                    CopyFoldersRecursively(dir, target.CreateSubdirectory(dir.Name));
                foreach (FileInfo file in source.GetFiles())
                    file.CopyTo(Path.Combine(target.FullName, file.Name));
            }
            catch
            {
                return false;
            }
            return true;
        }

        public static bool DeleteInFolder(string path)
        {
            if (!Directory.Exists(path))
            {
                return false;
            }
            DirectoryInfo di = new DirectoryInfo(path);
            try
            {
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
                foreach (DirectoryInfo dir in di.GetDirectories())
                {
                    dir.Delete(true);
                }
            }
            catch
            {
                return false;
            }
            
            return true;
        }
    }
    
}
