﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using RoboSharp;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //StartupUri="MainWindow.xaml"
        private PathModel current;

        private Utilities.FileSerialization dataManager;
        private bool _dataLoaded, targetValid, sourceValid;
        private BindingList<string> _targetList = new BindingList<string>();
        //private copier copierAlert;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Quick_Mirror_Loaded(object sender, RoutedEventArgs e)
        {
            DuringMirror(false);
            LoadData();
            UpdateField();
            //copierAlert.CopyEventHandler += CopyCompleted; 
            validateControls(ref source, ref sOpen, ref sourceValid);
            validateControls(ref target, ref tOpen, ref targetValid);
            _targetList = new BindingList<string>(current.targets);
            TargetList.ItemsSource = _targetList;
            if (!current.aboutRead)
            {
                about.BorderBrush = Brushes.LightCoral;
            }
            _dataLoaded = true;
            validate();
            ScrollToBottom();
        }

        public void auto()
        {
            LoadData();
            mirror(current.source, current.targets);
        }
        public void LoadData()
        {
            dataManager = new Utilities.FileSerialization("data.data", PathModel.defaultModel());
            current = dataManager.load() as PathModel;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (_dataLoaded)
            {
                if (_targetList.Count > 0 && sourceValid)
                {
                    DuringMirror(true);
                    mirror(source.Text, new List<string>(_targetList));
                }
                else
                {
                    MessageBox.Show("Invalid paths or no targets.");
                }
            }
        }

        public void UpdateField()
        {
            source.Text = current.source;
            target.Text = current.target;
            //_targetList = new BindingList<string>(current.targets);
        }

        void SaveField()
        {
            if (_dataLoaded)
            {
                current.source = source.Text;
                current.target = target.Text;
                current.targets = new List<string>(_targetList);
                dataManager.save(current);
                UpdateField();
            }
        }
        private void source_TextChanged(object sender, TextChangedEventArgs e)
        {
            validate();
        }
        private void target_TextChanged(object sender, TextChangedEventArgs e)
        {
            validate();
        }

        void validate()
        {
            validateControls(ref source, ref sOpen, ref sourceValid);
            validateControls(ref target, ref tOpen, ref targetValid);
        }
        void validateControls(ref TextBox field, ref Button open, ref bool variable)
        {
            if (!_dataLoaded) return;
            if (!Directory.Exists(field.Text))
            {
                field.Background = Brushes.LightCoral;
                variable = false;
                open.IsEnabled = false;
            }
            else
            {
                field.Background = Brushes.White;
                variable = true;
                open.IsEnabled = true;
            }
            SaveField();
        }
        

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            MessageBox.Show(
                "This application is use to mirror a source to target folders. Field will become red if path is invalid. Locked files will be ignored. All settings are consistantly saved. MIRRORING WILL PROCESS WITHOUT WARNING!"+
                Environment.NewLine + Environment.NewLine +
                "Use parameter \"/a\" and set the working path for silent mode using Task Scheduler.");
            current.aboutRead = true;
            about.BorderBrush = Brushes.Gray;
            SaveField();
        }

        void DuringMirror(bool isMirroring)
        {
            if (!isMirroring)
            {
                mmessage.Visibility = Visibility.Hidden;
            }
            else
            {
                mmessage.Visibility = Visibility.Visible;
            }
            Button_Mirror.IsEnabled = !isMirroring;
        }

        void WritePathToField(object sender, DragEventArgs e, string singlePath = "")
        {
            if (singlePath != "")
            {
                if (!_targetList.Contains(singlePath))
                {
                    _targetList.Add(singlePath);
                }
                return;
            }
            var senderObj = sender as TextBox;
            var name = senderObj.Name;
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                var data = e.Data.GetData(DataFormats.FileDrop) as string[];
                if (data.Length > 0)
                {
                    if (name == "source")
                    {
                        source.Text = data[0];
                    }
                    else
                    {
                        target.Text = data[0];
                        foreach (var s in data)
                        {
                            if (!Directory.Exists(s))
                            {
                                //r.v("Invalid path: " + s);
                            }
                            else
                            {
                                if (!_targetList.Contains(s))
                                {
                                    _targetList.Add(s);
                                }
                            } 
                        }
                    }
                }
            }
            ScrollToBottom();
            SaveField();
        }

        void ScrollToBottom()
        {
            if (TargetList.Items.Count < 1) return;
            TargetList.ScrollIntoView(TargetList.Items[TargetList.Items.Count - 1]);
        }

        void mirror(string source, List<string> Targets)
        {
            foreach (var target in Targets)
            {
                var mirror = new RoboCommand(); //this is already async
                if (source != target)
                {
                    mirror.CopyOptions.Mirror = true;
                    mirror.CopyOptions.Source = source;
                    mirror.CopyOptions.Destination = target;
                    mirror.RetryOptions.RetryCount = 1;
                    mirror.RetryOptions.RetryWaitTime = 2;
                    mirror.OnCommandError += FailedResult;
                    mirror.OnCommandCompleted += PassResult;
                    mirror.OnFileProcessed += progressReport;
                    mirror.Start();
                }
            }
        }

        void progressReport(object o, RoboSharp.FileProcessedEventArgs report)
        {
            var fileName = report.ProcessedFile.Name;
            var fileClass = report.ProcessedFile.FileClass;
            //Console.WriteLine(fileName + " <-- file name : file class--> " + fileClass);
            if (fileClass == "New File")
            {
                Dispatcher.BeginInvoke((Action)(() =>
                {
                    if (App.AutomationMode)
                    {
                        Console.WriteLine("Mirroring: " + fileName);
                    }
                    else
                    {
                        mmessage.Content = "Mirroring: " + fileName;
                    }       
                }));
            }
        }

        private void sOpen_Click(object sender, RoutedEventArgs e)
        {
            Process.Start(source.Text);
        }

        private void tOpen_Click(object sender, RoutedEventArgs e)
        {
            Process.Start(target.Text);
        }

        private void source_Drop(object sender, DragEventArgs e)
        {
            e.Effects = DragDropEffects.All;
            e.Handled = true;
            Console.WriteLine("Source has been dropped");
            WritePathToField(sender, e);
        }

        private void remove_Click(object sender, RoutedEventArgs e)
        {
            var queueRemove = new List<string>();
            foreach (var targetListSelectedItem in TargetList.SelectedItems)
            {
                queueRemove.Add(targetListSelectedItem.ToString());
            }
            foreach (var s in queueRemove)
            {
                _targetList.Remove(s);
            }
            SaveField();
        }

        private void tInclude_Click(object sender, RoutedEventArgs e)
        {
            validate();
            if (targetValid)
            {
                WritePathToField(null, null, target.Text);
                SaveField();
            }
            else
            {
                MessageBox.Show("Target is invalid");
            }
            
        }

        void FailedResult(object o, RoboSharp.ErrorEventArgs result)
        {
            Console.WriteLine("Mirroring was NOT successful");
            if(!App.AutomationMode) MessageBox.Show("Mirroring has failed");
            Dispatcher.BeginInvoke((Action)(() =>
            {
                if (!App.AutomationMode)
                {
                    DuringMirror(false);
                }
                else
                {
                    Environment.Exit(0);
                }
            }));
        }

        void PassResult(object o, RoboSharp.RoboCommandCompletedEventArgs result)
        {
            Console.WriteLine("Mirroring was successful");
            Dispatcher.BeginInvoke((Action)(() =>
            {
                if (!App.AutomationMode)
                {
                    DuringMirror(false);
                }
                else
                {
                    Environment.Exit(0);
                }
            }));
        }

    }
}
