﻿using System;
using System.Windows;
namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static bool AutomationMode = false;
        void App_Startup(object sender, StartupEventArgs e)
        {
            //AutomationMode = true;
            // Application is running
            // Process command line args
            for (int i = 0; i != e.Args.Length; ++i)
            {
                if (e.Args[i] == "/a")
                {
                    Console.WriteLine("Auto mode");
                    AutomationMode = true;
                }
            }
            var main = new MainWindow();
            if (!AutomationMode)
            {
                main.ShowDialog();
            }
            else
            {
                Console.WriteLine("Loading Data");
                main.LoadData();
                Console.WriteLine("Mirroring");
                main.auto();
            }
            
        }
    }
}
