# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###
This is application is use to mirror a source folder to many target folder. Folder mirroring is a common feature in many backup software, but the purpose of this application is to be able to setup a configuration as quickly as possible.

-Path validation

-Path drag and drop

-Path drag and drop to multiple targets

-Session settings are always saved

-Performs mirroring in parallel

-Works with Task Scheduler for silent operation

-Portable - The entire application is only one exe file. It uses only one config file that will auto-generate in the application's path.

-The application works as it's own profile - just copy this application to set up another profile.

![Scheme](/0.jpg)

### How do I get set up? ###

Just download the compiled version: https://drive.google.com/open?id=1jBX7pjK3dnUEI6F4iNW9i-ZKHA2CkkZG

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact